package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class MySistro extends ApplicationAdapter {
	SpriteBatch batch;
	private OrthographicCamera camera;
	Texture img_sistro, img_f1, img_f2, img_f3, img_f4;
	float widthScreen, heightScreen, cumAccX;
	private BitmapFont font;
	private Sprite sprite_f1, sprite_f2, sprite_f3, sprite_f4;
	private World world;
	private Body body_f1, body_f2, body_f3, body_f4, body_f1_collisione_dx, body_f2_collisione_dx, body_f3_collisione_dx,
			body_f4_collisione_dx, body_f1_collisione_sx, body_f2_collisione_sx, body_f3_collisione_sx, body_f4_collisione_sx,
			body_f1_collisione_sopra, body_f1_collisione_sotto, body_f2_collisione_sopra, body_f2_collisione_sotto,
			body_f3_collisione_sopra, body_f3_collisione_sotto, body_f4_collisione_sopra, body_f4_collisione_sotto;
	protected Box2DDebugRenderer debugRenderer;
	final float PIXELS_TO_METERS = 100f;
	Matrix4 debugMatrix;
	private static Sound wavSound_dx, wavSound_sx;
	private static String url_dx, url_sx;


	@Override
	public void create () {
		widthScreen = Gdx.graphics.getWidth();
		heightScreen = Gdx.graphics.getHeight();
		cumAccX = 0f;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 1080, 1920);
		camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
		camera.update();
		debugMatrix = new Matrix4(camera.combined);
		debugMatrix.scale(30f, 30f, 1f);
		batch = new SpriteBatch();
		world = new World(new Vector2(0, 0), true);
		debugRenderer = new Box2DDebugRenderer();
		url_dx = "sound/sistro_destra2.mp3";
		url_sx = "sound/sistro_sinistra2.mp3";
		wavSound_dx = Gdx.audio.newSound(Gdx.files.internal(url_dx));
		wavSound_sx = Gdx.audio.newSound(Gdx.files.internal(url_sx));

		img_sistro = new Texture("img/corpo_sistro_2.png");
		img_f1 = new Texture("img/1_ferretto.png");
		img_f2 = new Texture("img/2_ferretto.png");
		img_f3 = new Texture("img/3_ferretto.png");
		img_f4 = new Texture("img/4_ferretto.png");

		sprite_f1 = new Sprite(img_f1);
		sprite_f1.setPosition(150, 1355);

		sprite_f2 = new Sprite(img_f2);
		sprite_f2.setPosition(145, 1250);

		sprite_f3 = new Sprite(img_f3);
		sprite_f3.setPosition(135, 1150);

		sprite_f4 = new Sprite(img_f4);
		sprite_f4.setPosition(155,1025);


		// ferretto 1 partendo dall'alto basso
		setFerretto1();
		setFerretto2();
		setFerretto3();
		setFerretto4();

		font = new BitmapFont();
	}

	@Override
	public void render () {

		camera.update();
		batch.setProjectionMatrix(camera.combined);
		world.step(1/60f, 10, 10);
		world.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact)
			{
				if ((contact.getFixtureA().getBody() == body_f1_collisione_dx &&
						contact.getFixtureB().getBody() == body_f1))
				{
					Gdx.app.error("render()", "beginContact() f1-dx");
					//body_f1.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f1.getWorldCenter().x, body_f1.getWorldCenter().y, true);
					wavSound_dx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f1_collisione_sx &&
						contact.getFixtureB().getBody() == body_f1))
				{
					Gdx.app.error("render()", "beginContact() f1-sx");
					//body_f1.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f1.getWorldCenter().x, body_f1.getWorldCenter().y, true);
					wavSound_sx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f2_collisione_dx &&
						contact.getFixtureB().getBody() == body_f2))
				{
					Gdx.app.error("render()", "beginContact() f2-dx");
					//body_f2.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f2.getWorldCenter().x, body_f2.getWorldCenter().y, true);
					wavSound_dx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f2_collisione_sx &&
						contact.getFixtureB().getBody() == body_f2))
				{
					Gdx.app.error("render()", "beginContact() f2-sx");
					//body_f2.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f2.getWorldCenter().x, body_f2.getWorldCenter().y, true);
					wavSound_sx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f3_collisione_dx &&
						contact.getFixtureB().getBody() == body_f3))
				{
					Gdx.app.error("render()", "beginContact() f3-dx");
					//body_f3.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f3.getWorldCenter().x, body_f3.getWorldCenter().y, true);
					wavSound_dx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f3_collisione_sx &&
						contact.getFixtureB().getBody() == body_f3))
				{
					Gdx.app.error("render()", "beginContact() f3-sx");
					//body_f3.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f3.getWorldCenter().x, body_f3.getWorldCenter().y, true);
					wavSound_sx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f4_collisione_dx &&
						contact.getFixtureB().getBody() == body_f4))
				{
					Gdx.app.error("render()", "beginContact() f4-dx");
					//body_f4.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f4.getWorldCenter().x, body_f4.getWorldCenter().y, true);
					wavSound_dx.play();
					Gdx.input.vibrate(50);
				}
				else if ((contact.getFixtureA().getBody() == body_f4_collisione_sx &&
						contact.getFixtureB().getBody() == body_f4))
				{
					Gdx.app.error("render()", "beginContact() f4-sx");
					//body_f4.applyLinearImpulse(Gdx.input.getAccelerometerX(), 0, body_f4.getWorldCenter().x, body_f4.getWorldCenter().y, true);
					wavSound_sx.play();
					Gdx.input.vibrate(50);
				}
			}

			@Override
			public void endContact(Contact contact)
			{

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}
		});

		body_f1.applyForceToCenter(-(Gdx.input.getAccelerometerX() * 3f) * body_f1.getMass(), 0, true);
		body_f2.applyForceToCenter(-(Gdx.input.getAccelerometerX() * 3f) * body_f2.getMass(), 0, true);
		body_f3.applyForceToCenter(-(Gdx.input.getAccelerometerX() * 3f) * body_f3.getMass(), 0, true);
		body_f4.applyForceToCenter(-(Gdx.input.getAccelerometerX() * 3f) * body_f4.getMass(), 0, true);


		sprite_f1.setPosition((body_f1.getPosition().x * PIXELS_TO_METERS) - sprite_f1.getWidth()/2 , 1355);
		sprite_f2.setPosition((body_f2.getPosition().x * PIXELS_TO_METERS) - sprite_f2.getWidth()/2 , 1250);
		sprite_f3.setPosition((body_f3.getPosition().x * PIXELS_TO_METERS) - sprite_f3.getWidth()/2 , 1150);
		sprite_f4.setPosition((body_f4.getPosition().x * PIXELS_TO_METERS) - sprite_f4.getWidth()/2 , 1025);

		//Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(sprite_f1, sprite_f1.getX(), sprite_f1.getY(), img_f1.getWidth()/2.5f, img_f1.getHeight()/2);
		batch.draw(sprite_f2, sprite_f2.getX(), sprite_f2.getY(), img_f2.getWidth()/2.5f, img_f2.getHeight()/2);
		batch.draw(sprite_f3, sprite_f3.getX(), sprite_f3.getY(), img_f3.getWidth()/2.5f, img_f3.getHeight()/2);
		batch.draw(sprite_f4, sprite_f4.getX(), sprite_f4.getY(), img_f4.getWidth()/2.5f, img_f4.getHeight()/2);
		batch.draw(img_sistro, 0, 0, camera.viewportWidth, camera.viewportHeight);

		//debugRenderer.render(world, debugMatrix);

		batch.end();
	}

	@Override
	public void dispose () {
		batch.dispose();
		img_sistro.dispose();
		img_f1.dispose();
		img_f2.dispose();
		img_f3.dispose();
		img_f4.dispose();
		wavSound_dx.dispose();
		wavSound_sx.dispose();
		world.dispose();
		debugRenderer.dispose();
	}

	protected void setFerretto1()
	{
		BodyDef bodyDef_f1 = new BodyDef();
		bodyDef_f1.type = BodyDef.BodyType.DynamicBody;
		bodyDef_f1.position.set((sprite_f1.getX() + sprite_f1.getWidth()/2) /
						PIXELS_TO_METERS,
				//(sprite_f1.getY() + sprite_f1.getHeight()/2) / PIXELS_TO_METERS);
				5);
		body_f1 = world.createBody(bodyDef_f1);
		PolygonShape shape_f1 = new PolygonShape();
		shape_f1.setAsBox((sprite_f1.getWidth()/2)/PIXELS_TO_METERS, 1);
		shape_f1.dispose();
		FixtureDef fixtureDef_f1 = new FixtureDef();
		fixtureDef_f1.shape = shape_f1;
		fixtureDef_f1.density = 0.8f;
		fixtureDef_f1.friction = 0.5f;
		fixtureDef_f1.restitution = 0.1f; // Make it bounce a little bit
		body_f1.createFixture(fixtureDef_f1);

		BodyDef bodyDef_f1_collisione_sopra = new BodyDef();
		bodyDef_f1_collisione_sopra.type = BodyDef.BodyType.StaticBody;
		bodyDef_f1_collisione_sopra.position.set(body_f1.getWorldCenter().x, 6.1f);
		FixtureDef fixtureDef3_f1_sopra = new FixtureDef();
		EdgeShape edgeShape_f1_sopra = new EdgeShape();
		edgeShape_f1_sopra.set(-10 ,0,10 ,0);
		fixtureDef3_f1_sopra.shape = edgeShape_f1_sopra;
		body_f1_collisione_sopra = world.createBody(bodyDef_f1_collisione_sopra);
		body_f1_collisione_sopra.createFixture(fixtureDef3_f1_sopra);
		edgeShape_f1_sopra.dispose();

		BodyDef bodyDef_f1_collisione_sotto = new BodyDef();
		bodyDef_f1_collisione_sotto.type = BodyDef.BodyType.StaticBody;
		bodyDef_f1_collisione_sotto.position.set(body_f1.getWorldCenter().x, 3.9f);
		FixtureDef fixtureDef3_f1_sotto = new FixtureDef();
		EdgeShape edgeShape_f1_sotto = new EdgeShape();
		edgeShape_f1_sotto.set(-10 ,0,10 ,0);
		fixtureDef3_f1_sotto.shape = edgeShape_f1_sotto;
		body_f1_collisione_sotto = world.createBody(bodyDef_f1_collisione_sotto);
		body_f1_collisione_sotto.createFixture(fixtureDef3_f1_sotto);
		edgeShape_f1_sotto.dispose();

		BodyDef bodyDef_f1_collisione_dx = new BodyDef();
		bodyDef_f1_collisione_dx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f1_collisione_dx.position.set((camera.viewportWidth + 1110)/ PIXELS_TO_METERS, 5);
		FixtureDef fixtureDef3_f1_dx = new FixtureDef();
		EdgeShape edgeShape_f1_dx = new EdgeShape();
		edgeShape_f1_dx.set(0 ,- 3,0 ,3);
		fixtureDef3_f1_dx.shape = edgeShape_f1_dx;
		body_f1_collisione_dx = world.createBody(bodyDef_f1_collisione_dx);
		body_f1_collisione_dx.createFixture(fixtureDef3_f1_dx);
		edgeShape_f1_dx.dispose();

		BodyDef bodyDef_f1_collisione_sx = new BodyDef();
		bodyDef_f1_collisione_sx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f1_collisione_sx.position.set((30)/ PIXELS_TO_METERS, 5);
		FixtureDef fixtureDef3_f1_sx = new FixtureDef();
		EdgeShape edgeShape_f1_sx = new EdgeShape();
		edgeShape_f1_sx.set(0 , - 3,0 , 3);
		fixtureDef3_f1_sx.shape = edgeShape_f1_sx;
		body_f1_collisione_sx = world.createBody(bodyDef_f1_collisione_sx);
		body_f1_collisione_sx.createFixture(fixtureDef3_f1_sx);
		edgeShape_f1_sx.dispose();
	}

	protected void setFerretto2()
	{
		BodyDef bodyDef_f2 = new BodyDef();
		bodyDef_f2.type = BodyDef.BodyType.DynamicBody;
		bodyDef_f2.position.set((sprite_f2.getX() + sprite_f2.getWidth()/2)/PIXELS_TO_METERS, 15);
		body_f2 = world.createBody(bodyDef_f2);
		PolygonShape shape_f2 = new PolygonShape();
		shape_f2.setAsBox((sprite_f2.getWidth()/2)/PIXELS_TO_METERS, 1);

		shape_f2.dispose();
		FixtureDef fixtureDef_f2 = new FixtureDef();
		fixtureDef_f2.shape = shape_f2;
		fixtureDef_f2.density = 0.8f;
		fixtureDef_f2.friction = 0.5f;
		fixtureDef_f2.restitution = 0.1f; // Make it bounce a little bit
		body_f2.createFixture(fixtureDef_f2);

		BodyDef bodyDef_f2_collisione_sopra = new BodyDef();
		bodyDef_f2_collisione_sopra.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sopra.position.set(body_f2.getWorldCenter().x, 16.1f);
		FixtureDef fixtureDef3_f2_sopra = new FixtureDef();
		EdgeShape edgeShape_f2_sopra = new EdgeShape();
		edgeShape_f2_sopra.set(-10 ,0,10 ,0);
		fixtureDef3_f2_sopra.shape = edgeShape_f2_sopra;
		body_f2_collisione_sopra = world.createBody(bodyDef_f2_collisione_sopra);
		body_f2_collisione_sopra.createFixture(fixtureDef3_f2_sopra);
		edgeShape_f2_sopra.dispose();

		BodyDef bodyDef_f2_collisione_sotto = new BodyDef();
		bodyDef_f2_collisione_sotto.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sotto.position.set(body_f2.getWorldCenter().x, 13.9f);
		FixtureDef fixtureDef3_f2_sotto = new FixtureDef();
		EdgeShape edgeShape_f2_sotto = new EdgeShape();
		edgeShape_f2_sotto.set(-10 ,0,10 ,0);
		fixtureDef3_f2_sotto.shape = edgeShape_f2_sotto;
		body_f2_collisione_sotto = world.createBody(bodyDef_f2_collisione_sotto);
		body_f2_collisione_sotto.createFixture(fixtureDef3_f2_sotto);
		edgeShape_f2_sotto.dispose();

		BodyDef bodyDef_f2_collisione_dx = new BodyDef();
		bodyDef_f2_collisione_dx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_dx.position.set((camera.viewportWidth + 1098)/ PIXELS_TO_METERS, 15);
		FixtureDef fixtureDef3_f2_dx = new FixtureDef();
		EdgeShape edgeShape_f2_dx = new EdgeShape();
		edgeShape_f2_dx.set(0 ,-3,0 ,3);
		fixtureDef3_f2_dx.shape = edgeShape_f2_dx;
		body_f2_collisione_dx = world.createBody(bodyDef_f2_collisione_dx);
		body_f2_collisione_dx.createFixture(fixtureDef3_f2_dx);
		edgeShape_f2_dx.dispose();

		BodyDef bodyDef_f2_collisione_sx = new BodyDef();
		bodyDef_f2_collisione_sx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sx.position.set((70)/ PIXELS_TO_METERS,
				//(sprite_f2.getY() + (sprite_f2.getHeight()/2) )/ PIXELS_TO_METERS);
				15);
		//bodyDef_f1_collisione_sx.position.set(-1100, sprite_f1.getY() + (sprite_f1.getHeight()/2));
		FixtureDef fixtureDef3_f2_sx = new FixtureDef();
		EdgeShape edgeShape_f2_sx = new EdgeShape();
		//edgeShape_f2_sx.set(0 ,- (sprite_f2.getHeight()/2)/PIXELS_TO_METERS,0 ,(sprite_f2.getHeight()/2)/PIXELS_TO_METERS);
		edgeShape_f2_sx.set(0 ,-3,0 ,3);
		fixtureDef3_f2_sx.shape = edgeShape_f2_sx;
		body_f2_collisione_sx = world.createBody(bodyDef_f2_collisione_sx);
		body_f2_collisione_sx.createFixture(fixtureDef3_f2_sx);
		edgeShape_f2_sx.dispose();
	}

	protected void setFerretto3()
	{
		BodyDef bodyDef_f3 = new BodyDef();
		bodyDef_f3.type = BodyDef.BodyType.DynamicBody;
		//bodyDef_f1.position.set(sprite_f1.getX(), sprite_f1.getY());
		bodyDef_f3.position.set((sprite_f3.getX() + sprite_f3.getWidth()/2) /
						PIXELS_TO_METERS,
				//(sprite_f3.getY() + sprite_f3.getHeight()/2 ) / PIXELS_TO_METERS);
				25);
		body_f3 = world.createBody(bodyDef_f3);
		PolygonShape shape_f3 = new PolygonShape();
		//shape_f1.setAsBox(sprite_f1.getWidth()/2, sprite_f1.getHeight()/2);
		shape_f3.setAsBox((sprite_f3.getWidth()/2)/PIXELS_TO_METERS, 1);

		shape_f3.dispose();
		FixtureDef fixtureDef_f3 = new FixtureDef();
		fixtureDef_f3.shape = shape_f3;
		fixtureDef_f3.density = 0.8f;
		fixtureDef_f3.friction = 0.5f;
		fixtureDef_f3.restitution = 0.1f; // Make it bounce a little bit
		body_f3.createFixture(fixtureDef_f3);
		//body_f1.setLinearVelocity(-(Gdx.input.getAccelerometerX())*100,0);


		BodyDef bodyDef_f2_collisione_sopra = new BodyDef();
		bodyDef_f2_collisione_sopra.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sopra.position.set(body_f2.getWorldCenter().x, 26.1f);
		FixtureDef fixtureDef3_f2_sopra = new FixtureDef();
		EdgeShape edgeShape_f2_sopra = new EdgeShape();
		edgeShape_f2_sopra.set(-10 ,0,10 ,0);
		fixtureDef3_f2_sopra.shape = edgeShape_f2_sopra;
		body_f3_collisione_sopra = world.createBody(bodyDef_f2_collisione_sopra);
		body_f3_collisione_sopra.createFixture(fixtureDef3_f2_sopra);
		edgeShape_f2_sopra.dispose();

		BodyDef bodyDef_f2_collisione_sotto = new BodyDef();
		bodyDef_f2_collisione_sotto.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sotto.position.set(body_f2.getWorldCenter().x, 23.9f);
		FixtureDef fixtureDef3_f2_sotto = new FixtureDef();
		EdgeShape edgeShape_f2_sotto = new EdgeShape();
		edgeShape_f2_sotto.set(-10 ,0,10 ,0);
		fixtureDef3_f2_sotto.shape = edgeShape_f2_sotto;
		body_f3_collisione_sotto = world.createBody(bodyDef_f2_collisione_sotto);
		body_f3_collisione_sotto.createFixture(fixtureDef3_f2_sotto);
		edgeShape_f2_sotto.dispose();

		BodyDef bodyDef_f3_collisione_dx = new BodyDef();
		bodyDef_f3_collisione_dx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f3_collisione_dx.position.set((camera.viewportWidth + 1080)/ PIXELS_TO_METERS,
				//(sprite_f3.getY() + (sprite_f3.getHeight()/2) )/ PIXELS_TO_METERS);
				25);
		//bodyDef_f1_collisione_dx.position.set(widthScreen + 160, sprite_f1.getY() + (sprite_f1.getHeight()/2));
		FixtureDef fixtureDef3_f3_dx = new FixtureDef();
		EdgeShape edgeShape_f3_dx = new EdgeShape();
		//edgeShape_f3_dx.set(0 ,- (sprite_f3.getHeight()/2)/PIXELS_TO_METERS,0 ,(sprite_f3.getHeight()/2)/PIXELS_TO_METERS);
		edgeShape_f3_dx.set(0 ,-3,0 ,3);
		//edgeShape_f2_dx.set(0 , - 2,0 , 2);
		fixtureDef3_f3_dx.shape = edgeShape_f3_dx;
		body_f3_collisione_dx = world.createBody(bodyDef_f3_collisione_dx);
		body_f3_collisione_dx.createFixture(fixtureDef3_f3_dx);
		edgeShape_f3_dx.dispose();

		BodyDef bodyDef_f3_collisione_sx = new BodyDef();
		bodyDef_f3_collisione_sx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f3_collisione_sx.position.set((60)/ PIXELS_TO_METERS,
				//(sprite_f3.getY() + (sprite_f3.getHeight()/2) )/ PIXELS_TO_METERS);
				25);
		//bodyDef_f1_collisione_sx.position.set(-1100, sprite_f1.getY() + (sprite_f1.getHeight()/2));
		FixtureDef fixtureDef3_f3_sx = new FixtureDef();
		EdgeShape edgeShape_f3_sx = new EdgeShape();
		//edgeShape_f3_sx.set(0 ,- (sprite_f3.getHeight()/2)/PIXELS_TO_METERS,0 ,(sprite_f3.getHeight()/2)/PIXELS_TO_METERS);
		edgeShape_f3_sx.set(0 ,-3,0 ,3);
		fixtureDef3_f3_sx.shape = edgeShape_f3_sx;
		body_f3_collisione_sx = world.createBody(bodyDef_f3_collisione_sx);
		body_f3_collisione_sx.createFixture(fixtureDef3_f3_sx);
		edgeShape_f3_sx.dispose();
	}

	protected void setFerretto4()
	{
		BodyDef bodyDef_f4 = new BodyDef();
		bodyDef_f4.type = BodyDef.BodyType.DynamicBody;
		//bodyDef_f1.position.set(sprite_f1.getX(), sprite_f1.getY());
		bodyDef_f4.position.set((sprite_f4.getX() + sprite_f4.getWidth()/2) /
						PIXELS_TO_METERS,
				//(sprite_f4.getY() + sprite_f4.getHeight()/2) / PIXELS_TO_METERS);
				35);
		body_f4 = world.createBody(bodyDef_f4);
		PolygonShape shape_f4 = new PolygonShape();
		//shape_f1.setAsBox(sprite_f1.getWidth()/2, sprite_f1.getHeight()/2);
		shape_f4.setAsBox((sprite_f4.getWidth()/2)/PIXELS_TO_METERS, 1);

		shape_f4.dispose();
		FixtureDef fixtureDef_f4 = new FixtureDef();
		fixtureDef_f4.shape = shape_f4;
		fixtureDef_f4.density = 0.8f;
		fixtureDef_f4.friction = 0.5f;
		fixtureDef_f4.restitution = 0.1f; // Make it bounce a little bit
		body_f4.createFixture(fixtureDef_f4);
		//body_f1.setLinearVelocity(-(Gdx.input.getAccelerometerX())*100,0);


		BodyDef bodyDef_f2_collisione_sopra = new BodyDef();
		bodyDef_f2_collisione_sopra.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sopra.position.set(body_f2.getWorldCenter().x, 36.1f);
		FixtureDef fixtureDef3_f2_sopra = new FixtureDef();
		EdgeShape edgeShape_f2_sopra = new EdgeShape();
		edgeShape_f2_sopra.set(-8 ,0,8 ,0);
		fixtureDef3_f2_sopra.shape = edgeShape_f2_sopra;
		body_f4_collisione_sopra = world.createBody(bodyDef_f2_collisione_sopra);
		body_f4_collisione_sopra.createFixture(fixtureDef3_f2_sopra);
		edgeShape_f2_sopra.dispose();

		BodyDef bodyDef_f2_collisione_sotto = new BodyDef();
		bodyDef_f2_collisione_sotto.type = BodyDef.BodyType.StaticBody;
		bodyDef_f2_collisione_sotto.position.set(body_f2.getWorldCenter().x, 33.9f);
		FixtureDef fixtureDef3_f2_sotto = new FixtureDef();
		EdgeShape edgeShape_f2_sotto = new EdgeShape();
		edgeShape_f2_sotto.set(-8 ,0,8 ,0);
		fixtureDef3_f2_sotto.shape = edgeShape_f2_sotto;
		body_f4_collisione_sotto = world.createBody(bodyDef_f2_collisione_sotto);
		body_f4_collisione_sotto.createFixture(fixtureDef3_f2_sotto);
		edgeShape_f2_sotto.dispose();

		BodyDef bodyDef_f4_collisione_dx = new BodyDef();
		bodyDef_f4_collisione_dx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f4_collisione_dx.position.set((camera.viewportWidth + 880)/ PIXELS_TO_METERS,
				//(sprite_f4.getY() + (sprite_f4.getHeight()/2))/ PIXELS_TO_METERS);
				35);
		//bodyDef_f1_collisione_dx.position.set(widthScreen + 160, sprite_f1.getY() + (sprite_f1.getHeight()/2));
		FixtureDef fixtureDef3_f4_dx = new FixtureDef();
		EdgeShape edgeShape_f4_dx = new EdgeShape();
		//edgeShape_f4_dx.set(0 ,- (sprite_f4.getHeight()/2)/PIXELS_TO_METERS,0 ,(sprite_f4.getHeight()/2)/PIXELS_TO_METERS);
		edgeShape_f4_dx.set(0 ,-3,0 ,3);
		//edgeShape_f2_dx.set(0 , - 2,0 , 2);
		fixtureDef3_f4_dx.shape = edgeShape_f4_dx;
		body_f4_collisione_dx = world.createBody(bodyDef_f4_collisione_dx);
		body_f4_collisione_dx.createFixture(fixtureDef3_f4_dx);
		edgeShape_f4_dx.dispose();

		BodyDef bodyDef_f4_collisione_sx = new BodyDef();
		bodyDef_f4_collisione_sx.type = BodyDef.BodyType.StaticBody;
		bodyDef_f4_collisione_sx.position.set((113)/ PIXELS_TO_METERS,
				//(sprite_f4.getY() + (sprite_f4.getHeight()/2))/ PIXELS_TO_METERS);
				35);
		//bodyDef_f1_collisione_sx.position.set(-1100, sprite_f1.getY() + (sprite_f1.getHeight()/2));
		FixtureDef fixtureDef3_f4_sx = new FixtureDef();
		EdgeShape edgeShape_f4_sx = new EdgeShape();
		//edgeShape_f4_sx.set(0 ,- (sprite_f4.getHeight()/2)/PIXELS_TO_METERS,0 ,(sprite_f4.getHeight()/2)/PIXELS_TO_METERS);
		edgeShape_f4_sx.set(0 ,-3,0 ,3);
		fixtureDef3_f4_sx.shape = edgeShape_f4_sx;
		body_f4_collisione_sx = world.createBody(bodyDef_f4_collisione_sx);
		body_f4_collisione_sx.createFixture(fixtureDef3_f4_sx);
		edgeShape_f4_sx.dispose();
	}
}