package com.mygdx.game;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class AboutActivity extends AppCompatActivity
{

    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Button play = (Button)findViewById(R.id.button_sound);
        Button restart = findViewById(R.id.button_restart);

        mp = MediaPlayer.create(this, R.raw.sistro_about);

        play.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mp.isPlaying())
                {
                    mp.pause();
                    play.setText(R.string.button_play_sound);
                } else
                {
                    mp.start();
                    play.setText(R.string.button_pause_sound);
                }
            }
        });

        restart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mp.seekTo(0);
            }
        });
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (mp != null)
        {
            mp.release();
            mp = null;
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
