package com.mygdx.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    Button play_sistro_button, sistro_wiki_button, button_about;
    Intent libgdx_sistro, sistro_wiki, sistro_about;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play_sistro_button = (Button)findViewById(R.id.button_play_sistro);
        sistro_wiki_button = (Button)findViewById(R.id.button_sistro_wiki);
        button_about = (Button)findViewById(R.id.button_about);
        libgdx_sistro = new Intent(this, AndroidLauncher.class);
        sistro_wiki = new Intent(this, webViewActivity.class);
        sistro_about = new Intent(this,AboutActivity.class);

        play_sistro_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivityForResult(libgdx_sistro, 0);
            }
        });

        sistro_wiki_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivityForResult(sistro_wiki, 0);
            }
        });

        button_about.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivityForResult(sistro_about, 0);
            }
        });

    }
}
